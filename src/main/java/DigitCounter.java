import java.util.ArrayList;
import java.util.Collections;

public class DigitCounter {

    private int number;

    DigitCounter(int number) {
        this.number = number;
    }

    public int countDigit(Digit digitToCount) {
        int count = 0;
        int rest = Math.abs(number);
        while (rest != 0) {
            int actualDigit = rest % 10;
            if (actualDigit == digitToCount.getValue()) {
                count++;
            }
            rest /= 10;
        }
        return count;
    }

    public boolean containsDigit(Digit digit) {
        return countDigit(digit) > 0;
    }

    public ArrayList<Integer> getDigits() {
        ArrayList<Integer> digits = new ArrayList<>();
        int rest = Math.abs(number);
        while (rest != 0) {
            int actualDigit = rest % 10;
            if (!digits.contains(actualDigit)) {
                digits.add(0, actualDigit);
            }
            rest /= 10;
        }
        Collections.sort(digits);
        return digits;
    }
}
